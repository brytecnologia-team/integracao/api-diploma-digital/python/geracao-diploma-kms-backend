from flask_cors import CORS
from flask import Flask, request

import sys
sys.path.append('./src/service/')
sys.path.append('./src/config/')
sys.path.append('./src/util/')

from ServiceConfig import ServiceConfig
from GeradorDiplomaService import GeradorDiplomaService
from copiaNodoRequestFromRequest import copiaNodoRequestFromRequest
from geradorDiplomaRequestFromRequest import geradorDiplomaRequestFromRequest

app = Flask(__name__)
CORS(app)
app.config['DOWNLOAD_FOLDER'] = 'Downloads'

@app.route('/**', methods=['OPTIONS'])
def optionsInicializaPublico():
    return '{\'Allow\':\"POST\", \"GET\", \"OPTIONS\", \"DELETE\", \"PATCH\", \"PUT"\"}'

@app.route('/XMLDiplomado/assinaKms', methods=['POST'])
def assinaKmsPublico():
    print('postInicializaPublico')
    geradorDiplomaRequest = geradorDiplomaRequestFromRequest(request, 'XMLDiplomado')

    response = GeradorDiplomaService().assinaXMLDiplomado(geradorDiplomaRequest)
    return response

@app.route('/XMLDocumentacaoAcademica/assinaKms', methods=['POST'])
def assinaKmsPrivado():
    print('postInicializaPrivado')
    geradorDiplomaRequest = geradorDiplomaRequestFromRequest(request, 'XMLDocumentacaoAcademica')

    response = GeradorDiplomaService().assinaXMLDocumentacao(geradorDiplomaRequest)
    return response

@app.route('/XMLDiplomado/copia-nodo', methods=['POST'])
def teste():
    copiaNodoRequest = copiaNodoRequestFromRequest(request)

    response = GeradorDiplomaService().copiaNodo(copiaNodoRequest)

    return response

if __name__ == '__main__':
    app.run(host="localhost", port=ServiceConfig.serverPort, debug=True)