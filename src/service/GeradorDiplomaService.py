from lib2to3.pgen2 import token
import requests
import pathlib
import base64
import json
import sys
import xml.dom.minidom
sys.path.append('./src/config/')

from ServiceConfig import ServiceConfig


class GeradorDiplomaService:
    authorization = ServiceConfig.authorization
    urlServico = ServiceConfig.urlServico

    def assinaXMLDiplomado(self, request):
        headers = self.getHeaders()
        form = self.getFormComun(request)
        files = self.getFiles(request)

        data = request.form

        # Recupera o tipo da credencial enviado (BRYKMS, DINAMO)
        kms_type = data["kms_type"]
        headers['kms_type'] = kms_type

        # Recupera o valor da credencial
        kms_credencial = data["valor_credencial"]

        # Tipo da credencial dentro do sistema de hospedagem de certificados (PIN, TOKEN ou OTP)
        kms_type_credential =  data['kms_type_credential'];

        # Dados das credenciais do KMS a serem enviados na requisição
        kms_data_info = ""

        # Recuperação dos dados de cada tipo de credencial
        if kms_type_credential == "PIN":
            kms_data_info =  '"pin" : "' + kms_credencial + '"'
        elif kms_type_credential == "TOKEN" :
            kms_data_info =  '"token" : "' + kms_credencial + '"'
        elif kms_type_credential == "OTP" :
            kms_data_info =  '"otp" : "' + kms_credencial + '"'

        dataKmsCredenciaisSistema = ""
        if kms_type == "DINAMO":
            if 'user_pkey' in data:
                dataKmsCredenciaisSistema = dataKmsCredenciaisSistema + ", " + '"user_pkey" : "' + data['user_pkey'] + '"'

        # Credenciais presentes em ambos BRYKMS e DINAMO
        if 'user' in data:
            dataKmsCredenciaisSistema = dataKmsCredenciaisSistema + ", " + '"user" : "' + data['user'] + '"'
        if 'uuid_cert' in data:
            dataKmsCredenciaisSistema = dataKmsCredenciaisSistema + ", " + '"uuid_cert" : "' + data['uuid_cert'] + '"'
        
        #Inserindo credenciais do KMS na Request
        kms_data_info = '{' + kms_data_info + dataKmsCredenciaisSistema + '}'
        form['kms_data'] = kms_data_info

        tipoAssinante = request.getTipoAssinante()
        if tipoAssinante == 'Representantes':
            print("Tipo de Assinatura: Pessoas Fisicas (Representantes)")
            form['profile'] = 'ADRC'
            form['originalDocuments[0][specificNode][namespace]'] = 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd'
            #Pode ser necessário alterar este parâmetro para DadosRegistroNSF de acordo com o arquivo sendo utilizado.
            form['originalDocuments[0][specificNode][name]'] = 'DadosRegistro'
        elif tipoAssinante == 'IESRegistradora':
            print("Tipo de Assinatura: IESRegistradora")
            form['profile'] = 'ADRA'
            form['includeXPathEnveloped'] = 'false'
        else:
            print('tipo de assinante não esperado:', tipoAssinante)
            return '{\"message\": \"Erro ao identificar o tipo de assinante em assinatura de Diploma: ' + tipoAssinante + '\"}'
        
        print('Iniciando processo de assinatura...')
        response = requests.request('POST', self.urlServico, headers=headers, data=form, files=files)

        if response.status_code != 200:
            print(response.text)
            return '{\"message\": \"Um erro não esperado aconteceu: ' + json.loads(response.text)['message'] + '\"}'
        
        return self.finalizar(request, response)

    def assinaXMLDocumentacao(self, request):
        headers = self.getHeaders()
        form = self.getFormComun(request)
        files = self.getFiles(request)

        tipoAssinante = request.getTipoAssinante()
        if tipoAssinante == 'Representantes':
            print("Tipo de Assinatura: Decano ou Reitor (Representantes)")
            form['profile'] = 'ADRC'
            form['originalDocuments[0][specificNode][namespace]'] = 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd'
            #Pode ser necessário alterar este parâmetro para DadosDiplomaNSF de acordo com o arquivo sendo utilizado.
            form['originalDocuments[0][specificNode][name]'] = 'DadosDiploma'
        elif tipoAssinante == 'IESEmissoraDadosDiploma':
            print("Tipo de Assinatura: IESEmissora em Dados Diploma")
            form['profile'] = 'ADRC'
            form['originalDocuments[0][specificNode][namespace]'] = 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd'
            #Pode ser necessário alterar este parâmetro para DadosDiplomaNSF de acordo com o arquivo sendo utilizado.
            form['originalDocuments[0][specificNode][name]'] = 'DadosDiploma'
            form['includeXPathEnveloped'] = 'false'
        elif tipoAssinante == 'IESEmissoraRegistro':
            print("Tipo de Assinatura: IESEmissora para Registro")
            form['profile'] = 'ADRA'
            form['includeXPathEnveloped'] = 'false'
        else:
            print('tipo de assinante não esperado:', tipoAssinante)
            return '{\"message\": \"Erro ao identificar o tipo de assinante em assinatura de Documentação: ' + tipoAssinante + '\"}'   
            
        print('Iniciando processo de assinatura...')
        response = requests.request('POST', self.urlServico, headers=headers, data=form, files=files)

        if response.status_code != 200:
            print(response.text)
            return '{\"message\": \"Um erro não esperado aconteceu: ' + json.loads(response.text)['message'] + '\"}'
        
        return self.finalizar(request, response)
    
    def getHeaders(self):        
        headers = {}
        headers['Authorization'] = self.authorization

        return headers

    def getFormComun(self, request):        
        form = {}
        form['nonce'] = 1
        form['signatureFormat'] = 'ENVELOPED'
        form['hashAlgorithm'] = 'SHA256'
        form['returnType'] = request.getReturnType()
        form['originalDocuments[0][nonce]'] = 0 

        return form

    def getFiles(self, request):
        file = open(request.getFilePath(),'rb')
        files = {
                    'originalDocuments[0][content]' : file
                }

        return files

    def finalizar(self, request, response):        
        print('Assinatura realizada com sucesso!')
        if request.getReturnType() == 'LINK':
            print('Retornando link para download')
            return response.json()
        else:
            jsonResponse = response.json()
            content = jsonResponse[0]

            decodedContent = base64.b64decode(content)
            full_path = str(pathlib.Path().resolve()) + '/XMLsAssinados/exemplo-assinado.xml'
            diploma = open(full_path,'w')
            diploma.write(decodedContent.decode("utf-8"))
            diploma.close()

            print('Arquivo salvo localmente em: ' + full_path)
            return '{\"message\": \"Diploma armazenado localmente em: ' + full_path + '\"}'

    #Copia o Nodo DadosDiploma do XML de DocumentacaoAcademica para imediatamente antes do nodo Dados Registro do XML do Diplomado
    def copiaNodo(self, request):
        print("Requisição recebida: \n", request)

        DOMTreeDiplomado = xml.dom.minidom.parse(request.getPathXMLDiplomado())
        DOMTreeDocumentacao = xml.dom.minidom.parse(request.getPathDocumentacaoAcademica())

        documentElementDiplomado = DOMTreeDiplomado.documentElement
        documentElementDocumentacao = DOMTreeDocumentacao.documentElement

        dadosDiplomaNodeList = documentElementDocumentacao.getElementsByTagName('DadosDiploma')
        if dadosDiplomaNodeList.length == 0:
            dadosDiplomaNodeList = documentElementDocumentacao.getElementsByTagName('DadosDiplomaNSF')

            if dadosDiplomaNodeList.length == 0:
                return "{\"message\":\"Erro ao recuperar  Nodo DadosDiploma\"}"

        dadosDiplomaNode = dadosDiplomaNodeList.item(0)

        infDiplomaNode = documentElementDiplomado.getElementsByTagName('infDiploma').item(0)
        dadosRegistroNode = documentElementDiplomado.getElementsByTagName('DadosRegistro').item(0)
        infDiplomaNode.insertBefore(dadosDiplomaNode, dadosRegistroNode)

        if request.getReturnType() == "LOCAL":
            full_path = str(pathlib.Path().resolve()) + '/XMLsAssinados/exemplo-copia-nodo.xml'
            file = open(full_path,'w')
            file.write(DOMTreeDiplomado.toxml())
            file.close()
            return '{\"message\": \"Arquivo armazenado localmente em: ' + full_path + '\"}'
        else:
            return DOMTreeDiplomado.toxml()