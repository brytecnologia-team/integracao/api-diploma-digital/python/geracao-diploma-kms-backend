import pathlib

class ServiceConfig:
    serverPort = '3333'
    urlServico = 'https://diploma.hom.bry.com.br/api/xml-signature-service/v1/signatures/kms'
    #urlServico = 'https://diploma.bry.com.br/api/xml-signature-service/v1/signatures/kms'
    
    authorization = 'TOKEN_GERADO_NO_BRY_CLOUD'
    kmsCredencial = 'KMS_CREDENCIAL_EM_BASE_64'
    kmsCredencialTipo = 'PIN'

    caminhoDiploma = str(pathlib.Path().resolve()) + '/XMLsOriginais/exemplo-xml-diplomado.xml'
    caminhoDocumentacao = str(pathlib.Path().resolve()) + '/XMLsOriginais/exemplo-xml-documentacao.xml'

