class GeradorDiplomaRequest:
    def __init__(self, tipoAssinante, CPFAssinante, filePath, returnType, uuid):
        self.tipoAssinante = tipoAssinante
        self.CPFAssinante = CPFAssinante
        self.filePath = filePath
        self.returnType = returnType
        self.uuid = uuid
    
    def getTipoAssinante(self):
        return self.tipoAssinante

    def getCPFAssinante(self):
        return self.CPFAssinante

    def getFilePath(self):
        return self.filePath
    
    def getReturnType(self):
        return self.returnType

    def getUuid(self):
        return self.uuid