import sys
import pathlib
sys.path.append('./src/request/')
sys.path.append('./src/config/')
from werkzeug.utils import secure_filename
from GeradorDiplomaRequest import GeradorDiplomaRequest
from ServiceConfig import ServiceConfig

def geradorDiplomaRequestFromRequest(request, tipo=""):
    tipoAssinante = request.form['tipoAssinatura']
    cpf = request.form['signerKMS']
    uuid = request.form['uuid']

    file_path = ""
    returnType = ""
    if request.files:
        returnType = 'LINK'
        file = request.files['documento']
        filename = secure_filename(file.filename)
        file_path = str(pathlib.Path().resolve()) + '/Downloads/' + filename
        new_file = open(file_path,'wb')
        file.save(new_file)
        new_file.close()
    else:
        returnType = 'BASE64'
        if tipo == 'XMLDiplomado':
            file_path = ServiceConfig.caminhoDiploma
        else:
            file_path = ServiceConfig.caminhoDocumentacao

    return GeradorDiplomaRequest(tipoAssinante, cpf, file_path, returnType, uuid)
