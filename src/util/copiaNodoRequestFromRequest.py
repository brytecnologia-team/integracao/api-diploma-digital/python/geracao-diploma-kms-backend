import sys
import pathlib
sys.path.append('./src/request/')
sys.path.append('./src/config/')
from werkzeug.utils import secure_filename
from CopiaNodoRequest import CopiaNodoRequest
from ServiceConfig import ServiceConfig

def copiaNodoRequestFromRequest(request):
    pathDocumentacaoAcademica = ""
    pathXMLDiplomado = ""
    returnType = ""
    if request.files:
        returnType = 'xml'
        pathDocumentacaoAcademica = saveFile(request, 'documentacaoAcademica')
        pathXMLDiplomado = saveFile(request, 'XMLDiplomado')
    else:
        returnType = 'LOCAL'
        pathDocumentacaoAcademica = ServiceConfig.caminhoDocumentacao
        pathXMLDiplomado = ServiceConfig.caminhoDiploma

    return CopiaNodoRequest(pathDocumentacaoAcademica, pathXMLDiplomado, returnType)

def saveFile(request, fileKey):
        file = request.files[fileKey]
        filename = secure_filename(file.filename)
        file_path = str(pathlib.Path().resolve()) + '/Downloads/' + filename
        new_file = open(file_path,'wb')
        file.save(new_file)
        new_file.close()

        return file_path